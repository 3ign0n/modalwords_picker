#!/usr/bin/ruby

require 'optparse'
require 'rubygems'
require 'active_support/inflector'
require 'MeCab'
require 'sanitize'

OPT_KEY_LIMIT_COUNTS = 'counts'
OPT_KEY_EXCLUDES = 'excludes'
OPT_KEY_SCRIPTPATH = 'script_path'

def read_text file_name
  s = ''
  open(file_name, 'r') {|f|
    s = f.readlines.join
  }
  s
end

def create_exclude_list opts
  excludes_dir = File::expand_path 'excludes', opts[OPT_KEY_SCRIPTPATH]
  if opts.include? OPT_KEY_EXCLUDES
    excludes_dir = File::expand_path opt[OPT_KEY_EXCLUDES]
  end

  excludes = {}
  Dir::glob(excludes_dir + '/*').each {|file|
    words = create_word_hash file, {}
    excludes.merge! words
  }
  excludes
end

def create_word_hash file_name, excludes
  text = read_text file_name
  text = Sanitize.clean text
  splitted = text.split(/[\s,.;:`'?\(\)]+/)

  word_list = {}
  
  splitted.each {|word|
    if /[a-zA-Z]+/ =~ word
      w = word.downcase.singularize
      if !excludes.include? w
        if ! word_list.include? w
          word_list[w] = 1
        else
          word_list[w] += 1
        end
      end
    end
  }
  word_list
end

# def create_word_hash file_name, excludes
#   text = read_text file_name
#   node = MeCab::Tagger.new.parseToNode text

#   word_list = {}
  
#   while node do
#     word = node.surface
#     if /[a-zA-Z]+/ =~ word
#       w = word.downcase.singularize
#       if !excludes.include? w
#         if ! word_list.include? w
#           word_list[w] = 1
#         else
#           word_list[w] += 1
#         end
#       end
#     end
#     node = node.next
#   end
#   word_list
# end

def count_words file_name, excludes
  words = create_word_hash file_name, excludes
  sorted = words.sort_by {|key, value| -value}
end

def print_result words, opts
  i = 0;
  words.each{|word, count|
    if opts.include? OPT_KEY_LIMIT_COUNTS
      if opts[OPT_KEY_LIMIT_COUNTS] <= i
        break
      end
    end
    puts word.to_s + ': ' + count.to_s
    i += 1
  }
end

if __FILE__ == $0
  opts = {}

  opts[OPT_KEY_SCRIPTPATH] = File.expand_path(File.dirname(__FILE__))

  optp = OptionParser.new
  optp.on('-c counts') {|v| opts[OPT_KEY_LIMIT_COUNTS] = v.to_i }
  optp.on('-x exclude_dir') {|v| opts[OPT_KEY_EXCLUDES] = v }
  argv = optp.parse(ARGV)


  file_name = argv.shift
  excludes = create_exclude_list opts
  words = count_words file_name, excludes
  print_result words, opts
end
